# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Bowling.Repo.insert!(%Bowling.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Bowling.{Repo,Bowling}
#alias RepoName.{Repo,FolderName.FileName}
[%{name: "Shobit"},
%{name: "Fred"},
%{name: "John"},
%{name: "Gandhi"}]
|> Enum.map(fn bowling_data -> Bowling.changeset(%Bowling{}, bowling_data) end)
#|> Enum.map(fn TableName_data -> FileName.changeset(%FileName{}, TableName_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)
