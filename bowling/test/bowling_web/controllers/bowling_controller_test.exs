defmodule BowlingWeb.BowlingControllerTest do
  use BowlingWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Welcome to Phoenix!"
  end
  test "create player with acceptance" , %{conn: conn} do
    conn = post conn, "/start", %{name: "shobit123"}
    conn = get conn, redirected_to(conn)
    assert html_response(conn, 200) =~ ~r/Play your game/
  end
end
