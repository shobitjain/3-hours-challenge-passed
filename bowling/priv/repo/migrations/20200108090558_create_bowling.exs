defmodule Bowling.Repo.Migrations.CreateBowling do
  use Ecto.Migration

  def change do
    create table(:bowling) do
      add :name, :string

      timestamps()
    end

  end
end
