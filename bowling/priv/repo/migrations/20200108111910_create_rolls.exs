defmodule Bowling.Repo.Migrations.CreateRolls do
  use Ecto.Migration

  def change do
    create table(:rolls) do
      add :product_id, references(:bowling, on_delete: :nothing, type: :integer)
      add :roll1, :integer
      add :roll2, :integer
      add :roll3, :integer

      timestamps()
    end
    create unique_index(:rolls, [:email, :bowling_id], name: :unique_email_bowling_index)
  end
end
