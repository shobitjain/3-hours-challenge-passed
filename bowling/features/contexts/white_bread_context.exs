defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session
    %{}
  end
  scenario_finalize fn _status, _state ->
    #Hound.end_session
    nil
  end

  given_ ~r/^the following username is created$/, fn state ->
    navigate_to "/start"
    {:ok, state}
  end

  and_ ~r/^I want to play game as "(?<argument_one>[^"]+)"$/,
  fn state, %{argument_one: _argument_one} ->
    {:ok, state}
  end

  and_ ~r/^I enter name$/, fn state ->
    fill_field({:id, "name"}, "shobit")
    {:ok, state}
  end

  when_ ~r/^I submit the request$/, fn state ->
    click({:id, "submit_button"})
    {:ok, state}
  end

  then_ ~r/^I enter 'Roll1','Roll2' and 'Roll3'$/, fn state ->
    assert visible_in_page? ~r/Play your game/
    {:ok, state}
  end

  and_ ~r/^I save the value$/, fn state ->
    {:ok, state}
  end
  then_ ~r/^I should receive a score message$/, fn state ->
    {:ok, state}
  end
end
