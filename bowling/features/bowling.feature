Feature: bowling the game
As a user
Such That I go to play bowling game
I want to dice a the rolls

    Scenario: dice a roll with success
        Given the following username is created
        | name    |
        | shobit  | 
        | Fred    | 
        | John    | 
        | Gandhi  | 
    And I want to play game as "shobit"
    And I enter name
    When I submit the request
    Then I enter 'Roll1','Roll2' and 'Roll3'
    And I save the value
    Then I should receive a score message