defmodule BowlingWeb.BowlingController do
  use BowlingWeb, :controller
  alias Bowling.Repo
  alias Bowling.Bowling

  def index(conn, _params) do
    changeset = Bowling.changeset(%Bowling{}, %{})
    render conn, "index.html", changeset: changeset
  end

  def play(conn, _params) do
    render conn, "play.html"
  end


  def create(conn, %{"bowling" => user_params}) do
    changeset = Bowling.changeset(%Bowling{}, user_params)
    case Repo.insert(changeset) do
      {:ok, _post} ->
        conn
        |> put_flash(:info, "Play your game")
        |> redirect(to: bowling_path(conn, :play))
      {:error, _par} ->
        conn
        |> put_flash(:info, "Error in the input fields , please check")
        |> redirect(to: bowling_path(conn, :index))
    end
  end


end
