defmodule BowlingWeb.Router do
  use BowlingWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BowlingWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    resources "/start", BowlingController
    get "/play", BowlingController, :play

  end

  # Other scopes may use custom stacks.
  # scope "/api", BowlingWeb do
  #   pipe_through :api
  # end
end
