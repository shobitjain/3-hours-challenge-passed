defmodule Bowling.Rolls do
  use Ecto.Schema
  import Ecto.Changeset


  schema "rolls" do
    field :roll1, :integer
    field :roll2, :integer
    field :roll3, :integer
    belongs_to :product, Bowling.Bowling
    timestamps()
  end

  @doc false
  def changeset(rolls, attrs) do
    rolls
    |> cast(attrs, [:roll1, :roll2, :roll3])
    |> validate_required([:roll1, :roll2, :roll3])
  end
end
