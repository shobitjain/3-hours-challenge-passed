defmodule Bowling.Bowling do
  use Ecto.Schema
  import Ecto.Changeset


  schema "bowling" do
    field :name, :string
    has_many :rolls, Bowling.Rolls
    timestamps()
  end

  @doc false
  def changeset(bowling, attrs) do
    bowling
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name)
  end
end
